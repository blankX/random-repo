# How to get my GPG key

## The keyserver way:
 * Go to a keyserver (usually they sync, right? I use hkps://hkps.pool.sks-keyservers.net)
 * `gpg --recv-key 2C7F61F5`

## The keybase way (through website):
 *  Go to [my keybase account](https://keybase.io/blank_x)
 * Click on CC15 FC82 2C7F 61F5
 * Click on this key
 * Copy EVERYTHING IN IT
 * Paste it into a file (like blankX.asc) and save it
 * `gpg --import keyname`, like `gpg --import blankX.asc`

## The keybase way (through CLI):
 * `keybase pgp pull blank_x`

## The keybase way (through curl):
 * `curl https://keybase.io/blank_x/key.asc | gpg --import`

## The bother amolith way:
 * Ask amolith for my public key. His contact methods [here](https://nixnet.xyz/contact)

## The messages way on Telegram:
 * Go [here](https://t.me/blankX_chaos/1394), [here](https://t.me/blankX_chaos/1395) and [here](https://t.me/blankX_chaos/1396)
 * String them together in a file
 * `gpg --import keyname`, like `gpg --import blankX.asc`

## The go to my gitlab repo way:
 * Download [this file](https://gitlab.com/blankX/random-repo/raw/master/blankX.asc?inline=false)
 * `gpg --import keyname`, like `gpg --import blankX.asc`

## The go to my gitlab repo way (through curl):
 * `curl https://gitlab.com/blankX/random-repo/raw/master/blankX.asc | gpg --import`

## The file way on Telegram:
 * Download [this file](https://t.me/WindhoesRebel/1946)
 * `gpg --import keyname`, like `gpg --import blankX.asc`
