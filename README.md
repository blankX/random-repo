# About Me
I love Linux and python3, current distro is Linux Mint.

"Why do I have the reputation of stalking Ceda?"
"I stole the design of this README from [this README](https://github.com/Yasir-siddiqui/about-me/blob/master/README.md)."

## Contact Me
[contact.md](https://gitlab.com/blankX/random-repo/blob/master/contact.md)

## My Userbot
[Telegram Alt](https://t.me/theblankx) *(I didn't make it, [@baalajimaestro](https://t.me/baalajimaestro) did)*

## My Telegram Bots
0. [Self-hosted Questable Instance](https://t.me/Questable_created_by_ceda_ei_bot) *(it will go up and down, you have been warned)*
1. [Test Bot which has an actual bot in it](https://t.me/blankx_testbot) *(same warning from above and it responds only for people with perms)*

## My (crappy) channel
Did you know my PC's hostname is [WindhoesRebel](https://t.me/windhoesrebel)?
Link's right there, on [WindhoesRebel](https://t.me/windhoesrebel).

## My crappy and abandoned sticker packs
0. [blank X SECRET STICKERZ](https://telegram.me/addstickers/secretstickerzx)
1. [Emoji = Sticker](https://telegram.me/addstickers/emojistickerx) *(what a nice name)*

## My GPG key
[F8CF 5E33 2CB6 0B91 76EE  4884 CC15 FC82 2C7F 61F5](https://gitlab.com/blankX/random-repo/blob/master/get-keys.md)
